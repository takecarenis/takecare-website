﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace TakeCareSubscribeService.Controllers
{
    [RoutePrefix("api/service")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SubscribeController : ApiController
    {
        private string _connectionString = @"data source=den1.mssql7.gear.host; initial catalog=takecaredb; User Id=takecaredb; Password=Da6h_1QhHd!8";

        [HttpPost]
        [Route("subscribe")]
        public void Subscribe(string email)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                String query = "INSERT INTO dbo.SubscribedUsers (Email,Date) VALUES (@email,@dateTime)";

                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@email", email);
                    command.Parameters.AddWithValue("@dateTime", DateTime.Now);

                    connection.Open();
                    command.ExecuteNonQuery();
                }
            }
        }
    }
}
